"""
The following is an example of basic liquid handling using a between two vials using a disposable BD needle.

For convenience, components and items are profiled and stored in `ada_core.profiles`. The profiles are dictionaries
of keyword arguments required to describe the profiled component/item. To use these profiles, they may simply be
imported and handed directly to the class keyword arguments during instantiation.
"""

# instantiate vialtray
# import the Tray class, which is used to describe a tray of items
from ada_core.components.manipulated import Tray

# import a profile for the Tray instance
from ada_core.profiles.trays import vial_tray_gen1

# import a profile for the vials installed in the tray
from ada_core.profiles.vials import HPLC_2mL_pierce

# instantiate the vials
vialtray = Tray(
    # index location (this is used to procedurally calculate the location of the vials based on the profile
    location='O6',
    **vial_tray_gen1,  # apply profile
    itemkwargs=HPLC_2mL_pierce,  # use the following item profile
)

# instantiate needle tray
# the needle tray also uses the Tray class, only with a different profile
from ada_core.profiles.trays import needle_tray_gen2
needletray = Tray(
    location='C6',
    **needle_tray_gen2,
    itemkwargs={
        'gauge': 22,  # 22 gauge needles
        'bd_length': 50,  # 50 mm long BD needles
    }
)

# instantiate tip remover
# a needle remover will be needed to remove the disposable needles from the arm's probe
from ada_core.components.manipulated import TipRemover
from ada_core.profiles.tip_removers import integrated_gen2
remover = TipRemover(
    location='C6',  # this remover is integrated into the needle tray
    **integrated_gen2,
)


# instantiate sampling pump
# a pump connected to the arm's probe will be required for liquid manipulation
# Typically we use syringe pumps with an integrated valve to enable bidirectional flow
# The pump classes are structured so that the method calls are identical for all pumps (the code doesn't care what kind
# of pump is connected, although there are some pump-specific methods which are not globally supported).
from ada_core.components.pumps import CavroPump
from ada_core.profiles.syringes import UNF2a_2_5mL_3cm  # the installed syringe is a 2.5 mL, 3 cm syringe
cavro = CavroPump(
    comport=2,  # in our example, the Cavro pump is connected to comport 2 of the N9 controller
    # the Cavro pump requires that commands be prefixed with the address of the pump (this is physically set)
    address='/1',
    **UNF2a_2_5mL_3cm,
)

# this example loop will withdraw some sample from vial A1, transfer to vial A2, then make up A2 to a volume using the
# backing solvent of the pump

sourcevial = vialtray.get_item()  # this is a generator that returns the next vial in the tray (in this case A1)
targetvial = vialtray['A2']  # tray items may also be accessed via __getitem__ requests


# Getting a needle
needle = needletray.get_item()  # pull a needle instance form the needle tray
needle.retrieve_tip()  # retrieves the needle (installing it on the end of the robotarm probe)


# all class instances are interacting with a shared instance of the RobotArm class
# we will also need to interact with this instance, and it may be imported as follows
from ada_core.dependencies.arm import robotarm
# in many scripts, you will see the convenience assignment of the robotarm to n9
n9 = robotarm

# Move the needle to the source vial
# Robot movements operate in point space, and track the items installed-on or picked-up-by the arm.
# At this point, the needle is installed on the n9 probe, so when we call a movement command for the probe,
# it will move the tip of the needle to the specified location.
n9.move_to_location(  # this is the multi-purpose movement method for the robot arm
    sourcevial.get_pierce_location(),  # this retrieves a location appropriate for a needle to pierce
    target='probe',  # tells the command to move the probe (rather than the gripper) to this location
)

# Withdraw a sample plug
cavro.set_state('output')  # change the state of the pump valve to output (this is the typical state for our configurations)
cavro.start(
    0.05,  # the units of the pumps are in mL
    'pull',  # tells the pump to pull
    wait=True,  # waits for the pump to finish
)

# this is a convenience method for ensuring that the arm is above a certain height before performing another action
n9.safeheight(200.)

# move to the location of the target vial
n9.move_to_location(
    targetvial.get_pierce_location(),
    target='probe',
)

# empty the sample plug into the vial
cavro.start(
    0.05,
    'push',
    wait=2.,  # this will make the script wait an additional 2 seconds after the empty completes
    flowrate=5.,
)

# make up to 1.5 mL volume
cavro.draw_and_dispense(  # a convenience method for drawing from the input and expelling in the output for a pump
    1.5 - 0.05,
    wait=1.,
)

# remove the contaminated needle
n9.safeheight(200.)
remover.remove_tip()  # this method performs the actions necessary to remove the needle from the probe

