# Ada examples

This repository was created to provide some example scripts which use
the [`ada_core`](https://gitlab.com/ada-chem/ada_core/tree/stable)
library. These will hopefully serve as an illustration for how
to interact with and build scripts using `ada_core`. This is by no means
a comprehensive tutorial, as there are far too many classes and methods
to demonstrate each one without the example scripts becoming too complex
for illustrative purposes.

The general structure of `ada_core` is as follows: for every physical
item (vial, needle, etc.) or component (pump, tray, etc.) there is a
class parametertizing it. There are profiles provided for convenient
instantiation of those classes, and any locations are auto-generated
from a reference point provided on instantiation. Once instantiated,
any manipulations associated with the component/item are defined as
methods of those class instances. For example `Vial.pickup` is a method
that picks up a vial in the robot arm gripper, and `Pump.start` is a
method which starts a liquid handling pump.

Every method and class in `ada_core` has a descriptive docstring, and we
are working on expanding the how-to level documentation to make user's
lives easier.